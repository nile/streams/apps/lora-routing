# LoRa Routing

LoRa Routing is a streaming application that processes LoRa messages from all LoRa devices and forwards them to the
appropriate Kafka topic based on the application name specified in the message.

It basically is a demultiplexer that routes messages from the `lora-mqttq topic to the correct Kafka topic based on the
application name.

## Overview

### Owners & Administrators

| Title       | Details                                                                |
|-------------|------------------------------------------------------------------------|
| **E-group** | [nile-team](https://groups-portal.web.cern.ch/group/nile-team/details) |

## Kafka Topics

| Environment    | Topic Name                                                                                  |
|----------------|---------------------------------------------------------------------------------------------|
| **Production** | [lora-mqtt](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-mqtt)         |
| **Production** | [lora-mqtt-dlq](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-mqtt-dlq) |
| **QA**         | [lora-mqtt](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-mqtt)         |
| **QA**         | [lora-mqtt-dlq](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-mqtt-dlq) |

### Configuration

| Title                        | Details                                                                                  |
|------------------------------|------------------------------------------------------------------------------------------|
| **Configuration Repository** | [app-configs/lora-routing](https://gitlab.cern.ch/nile/streams/app-configs/lora-routing) |
