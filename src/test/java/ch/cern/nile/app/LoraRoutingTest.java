package ch.cern.nile.app;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.NoSuchElementException;
import java.util.Properties;

import com.google.gson.JsonObject;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;

import ch.cern.nile.common.configuration.properties.RoutingProperties;
import ch.cern.nile.test.utils.StreamTestBase;
import ch.cern.nile.test.utils.TestUtils;

@SuppressWarnings("PMD.AvoidDuplicateLiterals")
class LoraRoutingTest extends StreamTestBase {

    private static final String TEST_TOPIC_1 = "test-topic-1";
    private static final String TEST_TOPIC_2 = "test-topic-2";

    private static final JsonObject MATCHED_APP_1 = TestUtils.loadRecordAsJson("data/matched_application_1.json");
    private static final JsonObject MATCHED_APP_2 = TestUtils.loadRecordAsJson("data/matched_application_2.json");
    private static final JsonObject UNMATCHED_APP = TestUtils.loadRecordAsJson("data/unmatched_application.json");
    private static final JsonObject MISSING_APP_NAME = TestUtils.loadRecordAsJson("data/missing_application_name.json");

    @Override
    public LoraRouting createStreamInstance() {
        return new LoraRouting();
    }

    @Test
    void givenMatchedApp1_whenProcessed_thenRoutedToTopic1() {
        pipeRecord(MATCHED_APP_1);

        final ProducerRecord<String, JsonObject> outputRecord = readRecord(TEST_TOPIC_1);

        assertNotNull(outputRecord);

        assertThrows(NoSuchElementException.class, () -> {
            readRecord(TEST_TOPIC_2);
        });
    }

    @Test
    void givenMatchedApp2_whenProcessed_thenRoutedToTopic2DistinctOutputs() {
        pipeRecord(MATCHED_APP_2);

        final ProducerRecord<String, JsonObject> outputRecord2 = readRecord(TEST_TOPIC_2);
        assertNotNull(outputRecord2);

        assertThrows(NoSuchElementException.class, () -> {
            readRecord(TEST_TOPIC_1);
        });
    }

    @Test
    void givenMissingAppName_whenProcessed_thenNoOutput() {
        pipeRecord(MISSING_APP_NAME);
        assertThrows(NoSuchElementException.class, () -> {
            readRecord(TEST_TOPIC_1);
        });
    }

    @Test
    void givenUnmatchedApp_whenProcessed_thenRoutedToDLQNoOutputInTopic1() {
        pipeRecord(UNMATCHED_APP);

        final ProducerRecord<String, JsonObject> outputRecordDlq = readRecord(getDLQ_TOPIC());

        assertNotNull(outputRecordDlq);

        assertThrows(NoSuchElementException.class, () -> {
            readRecord(TEST_TOPIC_1);
        });
    }

    @Test
    void givenNonexistentConfig_whenConfigured_thenRuntimeException() {
        final Properties routingProperties = new Properties();
        routingProperties.put(RoutingProperties.ROUTING_CONFIG_PATH.getValue(), "not-existing");
        assertThrows(RuntimeException.class, () -> new LoraRouting().configure(routingProperties));
    }

}

