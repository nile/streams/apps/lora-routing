package ch.cern.nile.app.exceptions;


import java.io.Serial;

public class RoutingConfigurationException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 1L;

    public RoutingConfigurationException(final String message, final Throwable err) {
        super(message, err);
    }

}
