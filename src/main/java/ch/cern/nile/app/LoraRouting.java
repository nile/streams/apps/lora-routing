package ch.cern.nile.app;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Optional;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import ch.cern.nile.app.exceptions.RoutingConfigurationException;
import ch.cern.nile.common.configuration.properties.RoutingProperties;
import ch.cern.nile.common.json.JsonSerde;
import ch.cern.nile.common.models.Application;
import ch.cern.nile.common.streams.AbstractStream;

/**
 * LoraRouting is a Kafka Streams application that routes messages from a source topic to a sink topic based on the
 * configuration provided in the charts.
 */
public final class LoraRouting extends AbstractStream {

    private String dlqTopic;

    private Collection<Application> applications;

    @Override
    @SuppressFBWarnings(value = "PATH_TRAVERSAL_IN", justification = "Routing config path is trusted")
    public void configure(final Properties configs) {
        super.configure(configs);
        final String routingConfigPath =
                configs.getProperty(RoutingProperties.ROUTING_CONFIG_PATH.getValue());
        try {
            final byte[] bytes = Files.readAllBytes(Paths.get(routingConfigPath));
            final Type type = new TypeToken<Collection<Application>>() {
            }.getType();
            applications = new Gson().fromJson(new String(bytes, StandardCharsets.UTF_8), type);
        } catch (IOException cause) {
            throw new RoutingConfigurationException("Failed to read routing configuration file: " + routingConfigPath,
                    cause);
        }
        dlqTopic = configs.getProperty(RoutingProperties.DLQ_TOPIC.getValue());
    }

    @Override
    public void createTopology(final StreamsBuilder builder) {
        builder
                .stream(getSourceTopic(), Consumed.with(Serdes.String(), new JsonSerde()))
                .filter((key, value) -> value != null && getApplicationName(value) != null)
                .to((key, value, recordContext) -> {
                    final Optional<Application> application =
                            getFirstMatchingApplication(applications, getApplicationName(value));
                    return application.map(this::getApplicationTopicName).orElse(dlqTopic);
                });
    }

    /**
     * Gets the first Application object whose name matches the given applicationName.
     *
     * @param applications    - Collection of Applications
     * @param applicationName - The name of the Application
     * @return - Optional of {@link ch.cern.nile.common.models.Application}
     */
    private static Optional<Application> getFirstMatchingApplication(final Collection<Application> applications,
                                                                     final String applicationName) {
        return applications.stream().filter(application -> {
            try {
                return Pattern.compile(application.getName()).matcher(applicationName).matches();
            } catch (PatternSyntaxException e) {
                throw new IllegalArgumentException(e);
            }
        }).findFirst();
    }

    private String getApplicationName(final JsonObject value) {
        final JsonElement appNameElement = value.get("applicationName");
        return appNameElement != null ? appNameElement.getAsString() : null;
    }

    private String getApplicationTopicName(final Application application) {
        return application != null ? application.getTopic().getName() : null;
    }

}
